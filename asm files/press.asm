macro play note
	push ax
	
	in al, 61h
	or al, 00000011b
	out 61h, al	; turn on speakers
	
	mov al, [byte ptr note]	; port 42 is only 1byte, so sending 1 byte at a time
	out 42h, al
	mov al, [byte ptr note+1]
	out 42h, al
	
	pop ax
endm
	

; This proc is the main proc. It runs the whole project, and it's the only proc called in Main.asm.
; It checks for information from the keyboard, and based on the key plays a sound, changes an octave or exits.
last_pressed equ [byte ptr bp-1]
proc check
	push bp
	mov bp, sp
	sub sp, 1
	push ax	; save ax's value outside the proc
		
	bmp_process octave_pic
		
	checkKey:
		mov ax, 0C00h
		int 21h
		in al, 64h
		cmp al, 10b
		je checkKey	; checking if there's information from keyboard
		
		in al, 60h
		cmp al, 1
		je exitCheck	; if pressed key is ESC exit
		cmp al, 10h	; if pressed key is Q (octave change key)
		je octaves
		mov last_pressed, al	; save last key pressed
		
		and al, 80h
		jnz checkKey	; if key isn't pressed (released) keep checking
		
	ifPressed:
		call sounds
		jmp checkKey
	
	octaves:
		cmp last_pressed, 10h	; if key is already Q jump up (this makes Q a pulse key)
		je checkKey
		mov last_pressed, 10h	; if it isn't Q change octave and put Q into last_pressed
		call changeOctaves
		jmp checkKey
	
	exitCheck:
		pop ax
		add sp, 1
		pop bp
		ret
endp check
	
; this proc gets a scancode of a key and and playes a certain sound based on the key.
proc sounds
	push ax
	
	mov al, 0B6h
	out 43h, al
	
	check_key:
		in al, 60h
	    cmp al, 2Ch			; 
	    je caseZ    		 ; ; ;
	    cmp al, 2Dh 		  ;   ;
	    je caseX    		  ;   ;
	    cmp al, 2Eh 		  ;   ;
	    je caseC    		  ;   ; ; checking which key is pressed
	    cmp al, 2Fh 		  ;   ; ; switch(key)
	    je caseV    		  ;   ;
	    cmp al, 30h 		  ;   ;
	    je caseB    		 ; ; ;
	    jmp continue_check	;

	caseZ:
		play do		; do something
		jmp check_key
		jmp exitSounds	; break
	caseX:
		play re
		jmp check_key
		jmp exitSounds
	caseC:
		play mi
		jmp check_key
		jmp exitSounds
	caseV:
		play fa
		jmp check_key
		jmp exitSounds
	caseB:
		play sol
		jmp check_key
		jmp exitSounds
		
	continue_check:		; performing a middle-jump to continue checking
		cmp al, 31h
		je caseN    
		cmp al, 32h
		je caseM    
		cmp al, 33h
		je caseLast
		jmp exitSounds	
	
	caseN:
		play la
		jmp check_key
		jmp exitSounds
	caseM:
		play si_
		jmp check_key
		jmp exitSounds
	caseLast:	; the ,< key
		play do2
		jmp check_key
		jmp exitSounds
	; every time pushing to port 42h the divisor of wanted frequency
	
	exitSounds:
		in al, 61h
		and al, 11111100b
		out 61h, al	; turning off speakers
		
		pop ax
		ret
endp sounds

; This proc changes the octave number and tones, and prints a new BMP file based on octave.
proc changeOctaves
		push ax	; save ax
		
		; to calculate divisor we send   const   , and difference between octaves (same note) is times 2.
		;							   ---------
		;							   frequency
		; if we want to go to next ovctave, we divide by 2 (shr by 1). to go from 7 to 1 we multiply by 64 (shl by log2(64)).
		
		cmp [octave], 7	; if on last octave
		je reset
		shr [do],	1
		shr [re],	1
		shr [mi],	1
		shr [fa],	1
		shr [sol],	1
		shr [la],	1
		shr [si_],	1
		shr [do2],	1
		inc [octave]
		jmp exitChangeOctave
	reset:
		shl [do],	6
		shl [re],	6
		shl [mi],	6
		shl [fa],	6
		shl [sol],	6
		shl [la],	6
		shl [si_],	6
		shl [do2],	6
		mov [octave], 1
	exitChangeOctave:
		mov al, [octave]
		add al, 48	; al gets ascii of actave number
		mov [octave_pic+11], al	; changing the BMP filename
		bmp_process octave_pic	; printing BMP
		pop ax
		ret
endp changeOctaves

; This proc plays a note for a certain time
note equ [bp+6]	; note to play
ticks equ [bp+4]	; time to play note (in ticks)
clock equ es:6Ch
proc playNote_time
	push bp
	mov bp, sp
	push cx bx ax es
	
	xor dx, dx	; dx should be 0 unless ESC is pressed
	firstTick:
		mov ax, 40h	; seg of clock
		mov es, ax
		cmp [word ptr clock], ax
		je firstTick
	
	mov bx, note
	play bx	; play sound
	mov cx, ticks	; use ticks as loop counter
	
	delay:
		check_esc:
			mov ah, 1
			int 16h
			jz no_esc	; checking if there's information from keyboard
			
			mov ah, 0
			int 16h
			
			cmp ah, 01h
			je escPressed	; if pressed key is ESC exit
		
		no_esc:
			mov ax, [clock]
			tick:
				cmp ax, [clock]
				je tick
				loop delay	; loop until ticks have passed.
	jmp exit_playNote_time
	
	escPressed:
		mov dx, 1	; specify that we exited early
	exit_playNote_time:
		in al, 61h
		and al, 11111100b
		out 61h, al	; close speakers.
		
		pop es ax bx cx
		pop bp
		ret 4
endp playNote_time

; This proc plays a song arr (dw note, ticks). 
; It gets the offset to a song array and it's length in bytes
notes equ [bp+6]
len equ [bp+4]
proc playSong
	push bp
	mov bp, sp
	push si cx bx ax dx
	
	xor si, si
	xor ax, ax
	xor bx, bx
	xor cx, cx	; reset loop counter
	playnote:
		mov si, cx
		mov bx, notes
		push [bx+si]	; offset note
		push [bx+si+2]	; ticks
		
		xor dx, dx
		call playNote_time	; play for a certain time
		cmp dx, 1	; if exited early
		je exit_playSong
		
		push cx dx ax
		mov cx, 0h
		mov dx, 0C350h
		mov ah, 86h
		int 15h	; delay system for ~0.05sec
		pop ax dx cx
		
		add cx, 4	; jump to next line
		cmp cx, len
		jl playnote	; repeat until no more lines in array

	exit_playSong:
		mov ax, 0C00h
		int 21h
		pop dx ax bx cx si
		pop bp
		ret 4
endp playSong
