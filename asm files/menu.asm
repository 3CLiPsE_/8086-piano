; This proc handles the menu
proc menu
	push ax bx cx dx

	mov ax, 0
	int 33h	; reset mouse
	return_to_menu:
		bmp_process menu_pic
		mov ax, 1
		int 33h	; show mouse
		mov ax, 3
	check_press_menu:
		xor bx, bx	; reset bx every iteration
		int 33h	; check mouse info
		and bx, 00000001b
		jz check_press_menu	; if leftclick is pressed
		shr cx, 1	; x position is between 0-640, dividing by 2 to get 0-320
	check_boxes_menu:	; hitboxes for every choice
		cmp cx, 115
		jb check_press_menu
		cmp cx, 205
		ja check_press_menu
		cmp dx, 60
		jb check_press_menu
		cmp dx, 175
		ja check_press_menu
		cmp dx, 150
		ja exit_menu
		cmp dx, 145
		ja check_press_menu
		cmp dx, 120
		ja choice_premades
		cmp dx, 115
		ja check_press_menu
		cmp dx, 90
		ja choice_howto
		cmp dx, 85
		ja check_press_menu
		cmp dx, 60
		ja choice_start
		jmp check_press_menu
	
	choice_premades:
		mov ax, 2
		int 33h	; if we wont hide the mouse the picture wont print well
		call premades
		jmp return_to_menu
	choice_howto:
		mov ax, 2
		int 33h
		call howto
		jmp return_to_menu
	choice_start:
		mov ax, 2
		int 33h
		call check
		jmp return_to_menu
	
	exit_menu:
		pop dx cx bx ax
		ret
endp menu

; This proc handles the premades page
proc premades
	push ax bx cx dx

	bmp_process premade_pic	; print pic
	mov ax, 1
	int 33h	; show mouse
	
	mov cx, 7h
	mov dx, 0A120h
	mov ah, 86h
	int 15h		; system.wait for 500,000 microseconds (0.5 seconds)
	
	check_press_premades:
		mov ax, 3
		xor bx, bx
		int 33h	; check mouse info
		and bx, 00000001b
		jz check_press_premades	; if leftclick is pressed
		shr cx, 1	; x position is between 0-640, dividing by 2 to get 0-320
	check_back_box_premades:
		cmp dx, 15
		jb check_song_box_premades
		cmp dx, 32
		ja check_song_box_premades
		cmp cx, 248
		jb check_song_box_premades
		cmp cx, 312
		ja check_song_box_premades
		jmp exit_premades	; if clicked the "back" box
	check_song_box_premades:	; check hitboxes for every option
		cmp dx, 54
		jb check_press_premades
		cmp dx, 180
		ja check_press_premades
		cmp cx, 22
		jb check_press_premades
		cmp cx, 88
		jb num_1
		cmp cx, 130
		jb check_press_premades
		cmp cx, 196
		jb num_2
		cmp cx, 236
		jb check_press_premades
		cmp cx, 304
		jb num_3
		jmp check_press_premades
		
	num_1:
		push offset num1	; push song's offset
		push 96	; push song's length
		call playSong	; play the song of pressed box
		jmp check_press_premades	; continue checking
	num_2:
		push offset num2
		push 100
		call playSong
		jmp check_press_premades
	num_3:
		push offset num3
		push 104
		call playSong
		jmp check_press_premades
		
	exit_premades:
		mov ax, 2
		int 33h		; hide mouse
		pop dx cx bx ax
		ret
endp premades

; This proc handles the "How To" page
proc howto
	push ax bx cx dx

	bmp_process howto_pic
	mov ax, 1
	int 33h	; show mouse
	mov ax, 3
	check_press_howto:
		int 33h	; check mouse info
		and bx, 00000001b
		jz check_press_howto	; if leftclick is pressed
		shr cx, 1	; x position is between 0-640, dividing by 2 to get 0-320
	check_back_box_howto:
		cmp dx, 15
		jb check_press_howto
		cmp dx, 32
		ja check_press_howto
		cmp cx, 248
		jb check_press_howto
		cmp cx, 312
		ja check_press_howto
		jmp exit_howto	; if clicked the "back" box
	
	exit_howto:
		mov ax, 2
		int 33h
		pop dx cx bx ax
		ret
endp howto
