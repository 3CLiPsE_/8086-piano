; THIS WHOLE FILE EXCEPT FOR THE MACRO IS *NOT* WRITTEN BY ME. IT IS TAKEN FROM THE INTERNET (and the Gvahim book).

; Concats the file path with global path
file_path equ [bp+4]
proc makeFullPath
	push bp
	mov bp, sp
	push ax si di cx es
	
	mov ax, ds
	mov es, ax

    mov si, file_path
    mov di, offset pic_path
    add di, [path_len] ; Adding the length of destination string to the DI
	add di, 3
	sub di, 8
	
    mov cx, 17
    rep movsb ; This should concat two strings
	
	pop es cx di si ax
	pop bp
	ret 2
endp makeFullPath

; this macro is for processing a BMP file.
; it pushes the BMP file's name and calls all the procedures to print it.
macro bmp_process file
	push offset file
	call makeFullPath
	push offset pic_path
	call OpenFile
	call ReadHeader
	call ReadPalette
	call CopyPal
	call CopyBitmap
	call CloseFile
endm bmp_process

fileOffset equ [bp+4]
proc OpenFile
	push bp
	mov bp, sp
	; Open file
	mov ah, 3Dh
	xor al, al
	mov dx, fileOffset
	int 21h
	
	jc openerror
	mov [filehandle], ax
	jmp exitOpenFile
	
	openerror:
	mov dx, offset ErrorMsg
	mov ah, 9h
	int 21h
	
	exitOpenFile:
		pop bp
		ret 2
endp OpenFile

proc ReadHeader	
	; Read BMP file header, 54 bytes
	
	mov ah, 3Fh
	mov bx, [filehandle]
	mov cx, 54
	mov dx, offset Header
	int 21h
	ret
endp ReadHeader
	
proc ReadPalette	
	; Read BMP file color palette, 256 colors * 4 bytes (400h)
	
	mov ah, 3Fh
	mov cx, 400h
	mov dx, offset Palette
	int 21h
	ret
endp ReadPalette
	
proc CopyPal	
	; Copy the colors palette to the video memory
	; The number of the first color should be sent to port 3C8h
	; The palette is sent to port 3C9h
	
	mov si, offset Palette
	mov cx, 256
	mov dx, 3C8h
	mov al, 0
	
	; Copy starting color to port 3C8h
	
	out dx,al
	
	; Copy palette itself to port 3C9h
	
	inc dx
	PalLoop:
	
	; Note: Colors in a BMP file are saved as BGR values rather than RGB.
	
	mov al,[si+2] ; Get red value.
	shr al,2 ; Max. is 255, but video palette maximal
	
	; value is 63. Therefore dividing by 4.
	
	out dx,al ; Send it.
	mov al,[si+1] ; Get green value.
	shr al,2
	out dx,al ; Send it.
	mov al,[si] ; Get blue value.
	shr al,2
	out dx,al ; Send it.
	add si,4 ; Point to next color.
	
	; (There is a null chr. after every color.)
	
	loop PalLoop
	ret
endp CopyPal
	
proc CopyBitmap	
	; BMP graphics are saved upside-down.
	; Read the graphic line by line (200 lines in VGA format),
	; displaying the lines from bottom to top.
	
	mov ax, 0A000h
	mov es, ax
	mov cx, 200
	
	PrintBMPLoop:
	push cx
	
	; di = cx*320, point to the correct screen line
	
	mov di,cx
	shl cx,6
	shl di,8
	add di,cx
	
	; Read one line
	
	mov ah, 3Fh
	mov cx, 320
	mov dx, offset ScrLine
	int 21h
	
	; Copy one line into video memory
	
	cld 
	
	; Clear direction flag, for movsb
	
	mov cx, 320
	mov si, offset ScrLine
	rep movsb 
	
	;Copy line to the screen
	;rep movsb is same as the following code:
	;mov es:di, ds:si
	;inc si
	;inc di
	;dec cx
	;loop until cx=0
	
	pop cx
	loop PrintBMPLoop
	ret
endp CopyBitmap
proc CloseFile
	mov bx, [filehandle]
	mov ah, 3eh
	int 21h
	
	jc error_closefile
	jmp exit_closefile
	
	error_closefile:
		mov dx, offset ErrorMsg
		mov ah, 9h
		int 21h
		
	exit_closefile:
		ret
endp CloseFile
		