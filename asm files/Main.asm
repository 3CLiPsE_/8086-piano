IDEAL
MODEL small
STACK 100h
DATASEG
include "bmpDS.asm"
include "songs.asm"
	do 				dw 11D1h	;z
	re  			dw 0FDFh    ;x
	mi  			dw 0E24h    ;c
	fa  			dw 0D59h    ;v
	sol 			dw 0BE4h    ;b
	la  			dw 0A98h    ;n
	si_ 			dw 0970h    ;m
	do2 			dw 08E8h    ;,<
	octave			db 4
	octave_pic 		db 'BMPFIL~1\oc4.bmp', 0
	premade_pic 	db 'BMPFIL~1\pre.bmp', 0
	howto_pic 		db 'BMPFIL~1\how.bmp', 0
	menu_pic 		db 'BMPFIL~1\mnu.bmp', 0
	curr_path		db 256 dup (0)
	pic_path		db 'C', ':', '\', 250 dup (0)
	path_len		dw 0
	
CODESEG
include "bmpCS.asm"
include "press.asm"
include "menu.asm"
start:
	mov ax, @data
	mov ds, ax
	
	mov ah, 47h
	mov dl, 0
	mov si, offset curr_path
	int 21h
	
	mov bx, 0
check_len:
	mov al, [curr_path+bx]
	cmp al, 0
	je found_len
	inc bx
	jmp check_len
	
found_len:
	mov [path_len], bx
	
	mov ax, ds
	mov es, ax

    mov si, offset curr_path
    mov di, offset pic_path
    add di, 3 ; Adding the length of destination string to the DI
	
    mov cx, [path_len]
    rep movsb ; This should concat two strings
	
	; Graphic mode
	mov ax, 13h
	int 10h
	mov al, 0B6h
	out 43h, al	; getting permission
	
	call menu
exit:
	mov ah, 0
	mov al, 2
	int 10h	; exit graphic mode
	
	mov ax, 4c00h
	int 21h
END start
